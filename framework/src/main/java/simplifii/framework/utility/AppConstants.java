package simplifii.framework.utility;

import android.content.Intent;

import java.util.HashMap;
import java.util.LinkedHashMap;

public interface AppConstants {

    public static final String DEF_REGULAR_FONT = "OpenSans-Regular.ttf";
    String APP_LINK = "https://drive.google.com/file/d/0B8wKJnD6sONHeXlUbm5pOTk4dGM/view?usp=sharing";
    LinkedHashMap<Integer,String> storeCategory=new LinkedHashMap<Integer, String>();



    public static interface PARAMS {
        String LAT = "latitude";
        String LNG = "longitude";
        String POLLID = "pollId";
        String POLL_OPINION = "Poll Opinion";
    }
    public static interface TASK_CODES{

        int LOGIN = 10;
        int HOME = 11;
        int RESIDENT = 12;
        int FAMILY_MEMBER = 13;
        int CALENDER = 14;
        int INVITATION = 15;
        int NOTICEBOARD = 16;
        int PARKINGALLOTMENT = 17;
        int POLLDATA1 = 18;
        int POLLDATA2 = 19;
        int TENANT = 20;
        int VENDORHELP = 21;
        int COMMITTEESTAFF = 22;
        int SERVENTDRIVER = 23;

        int COMPLAINSTATUS = 24;

        int PHOTO = 24;
        int VIDEO = 25;
        int USER_PROFILE = 26;

        int EMERGENCY_LIST = 27;
        int EMERGENCY_ENTRY = 28;
        int COMPDROPLIST = 29;
        int SUBMIT_OPINION = 30;
        int MAINTENANCE = 31;
        int SEND_TICKET = 32;
    }

    public static interface TABLES_NAME{
        String RESIDENT = "mbCmembers";
        String FAMILY_MEMBERS = "fmCfamilymembers";
        String CALENDER = "caLcalendar";
    }
    public static interface ERROR_CODES {

        public static final int UNKNOWN_ERROR = 0;
        public static final int NO_INTERNET_ERROR = 1;
        public static final int NETWORK_SLOW_ERROR = 2;
        public static final int URL_INVALID = 3;
        public static final int DEVELOPMENT_ERROR = 4;

    }

    public static interface PAGE_URL {
        String _URL = "http://osoapi.azurewebsites.net/api/";
        String LOGIN_URL = "http://osoapi.azurewebsites.net/api/Login";
        String DASH_BOARD_URL = "http://osoapi.azurewebsites.net/api/Dashboard";
        String USER_PROFILE = "http://osoapi.azurewebsites.net/api/UserProfile";
        String BASE_URL = "http://osoapi.azurewebsites.net/api/operation";

        String COMPLAIN_STATUS = "http://osoapi.azurewebsites.net/api/ComplaintsStatusList";

        String PARKING_ALLOTMENT = "http://osoapi.azurewebsites.net/api/ParkingAllotment";

        String EASY_POLL_1 = "http://osoapi.azurewebsites.net/api/OpinionPoll";
        String VENDOR_HELP =" http://osoapi.azurewebsites.net/api/HelpList";
        String COMMITTEE_STAFF = "http://osoapi.azurewebsites.net/api/CommitteeStaffs";
        String POLL_RESULT = " http://osoapi.azurewebsites.net/api/OpinionPollResult";
        String INVITATION_LIST = "http://osoapi.azurewebsites.net/api/InvitationList";
        String PHOTO_LIST = "http://osoapi.azurewebsites.net/api/PhotoList";
        String VIDEO_LIST = "http://osoapi.azurewebsites.net/api/VideoList";

        String EMERGENCY_LIST = _URL + "EmergencyList";
        String EMERGENCY_ENTRY = _URL + "EmergencySingleEntry";
        String OPINION_POLL_SUBMIT = "http://osoapi.azurewebsites.net/api/OpinionPollSubmit";
        String SERVICE_STATUS = "http://osoapi.azurewebsites.net/api/ServiceStatus";
        String COMP_DROPDOWN = _URL + "CompType";
        String SEND_TICKET = "http://osoapi.azurewebsites.net/api/ComplaintsTicketLog";
    }

    public static interface PREF_KEYS {

        String ACCESS_CODE = "access";
        String APP_LINK = "appLink";


        String IS_LOGIN = "is_login";

        String USER_PROFILE = "user_profile";
        String EMAIL = "email";
        String NAME = "name";
        String ROLE="role";
    }

    public static interface BUNDLE_KEYS {
        public static final String KEY_SERIALIZABLE_OBJECT = "KEY_SERIALIZABLE_OBJECT";
        public static final String FRAGMENT_TYPE = "FRAGMENT_TYPE";
        String EXTRA_BUNDLE = "bundle";
        String NUMBER = "number";
        String SEARCH = "search";
        String MEMBER_POSITION = "memberPosition";
    }

    public static interface FRAGMENT_TYPE {
        int HOME_FRAGMENT = 0;
        int CALENDAR = 1;
        int EASY_POLL = 2;
        int DIRECTORY = 3;
        int FAMILY = 4;
        int NOTICE_BOARD = 5;
        int INVITATION = 6;
        int COMM_GALLERY = 7;
        int COMPLAINTS = 9;
        int MAINTAINENCE = 10;
        int VIDEO = 11;
        int PHOTOS = 12;
        int PARKING = 13;
        int LOGOUT = 14;
        int RESIDENTS = 15;
        int MY_PROFILE = 16;
    }


    public static interface VIEW_TYPE {
        int CARD_MY_TEAM = 0;
    }

}
