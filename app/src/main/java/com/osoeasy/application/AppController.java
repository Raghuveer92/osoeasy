package com.osoeasy.application;

import android.app.Application;

import simplifii.framework.utility.Preferences;

/**
 * Created by nitin on 10/04/16.
 */
public class AppController extends Application {
    private static AppController instance;

    public static AppController getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Preferences.initSharedPreferences(this);
    }
}
