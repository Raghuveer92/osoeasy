package com.osoeasy.activities;

import android.os.Bundle;

import com.osoeasy.R;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class SplashActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Thread(){
            @Override
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(Preferences.getData(AppConstants.PREF_KEYS.IS_LOGIN,false)){
                    startNextActivity(HomeActivity.class);
                }else {
                    startNextActivity(LoginActivity.class);
                }
                finish();
            }
        }.start();
    }
}
