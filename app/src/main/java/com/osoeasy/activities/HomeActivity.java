package com.osoeasy.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.osoeasy.fragments.DrawerFragment;
import com.osoeasy.R;
import com.osoeasy.fragments.HomeFragment;
import com.osoeasy.fragments.NextFragmentListener;
import com.osoeasy.models.UserProfile.UserProfileResponse;
import com.osoeasy.models.home.HomeResponce;
//import com.osoeasy.models.home.Timeline;

import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class HomeActivity extends BaseActivity implements NextFragmentListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        initToolBar("OSOEASY");
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_home);
        addDrawerFragment(DrawerFragment.getInstance(drawerLayout, this), R.id.lay_drawer);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextActivity(EmergencyListActivity.class);
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        addHomeFragment();
        getUserProfile();

    }

    private void getUserProfile() {
        HttpParamObject obj = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.USER_PROFILE, UserProfileResponse.class);
        executeTask(AppConstants.TASK_CODES.USER_PROFILE, obj);
    }

    private void addHomeFragment() {
        HomeFragment homeFragment = HomeFragment.getInstance();
        getSupportFragmentManager().beginTransaction().add(R.id.lay_fragment_container, homeFragment).addToBackStack(HomeFragment.class.getCanonicalName()).commit();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    private void addDrawerFragment(DrawerFragment drawerFragment, int lay_drawer) {
        getSupportFragmentManager().beginTransaction().add(lay_drawer, drawerFragment).commit();
    }

    private void addFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.replace(R.id.lay_fragment_container, fragment).commit();

    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 1) {
            finish();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void startNext(Fragment fragment) {
        if (fragment == null) {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            if (count < 2) {
//                onBackPressed();
            } else {
                getSupportFragmentManager().popBackStack(HomeFragment.class.getCanonicalName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                addHomeFragment();
            }
        } else {
            addFragment(fragment, true);
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
    }
}

