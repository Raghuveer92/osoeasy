package com.osoeasy.fragments;

import android.app.FragmentManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.NoticeBoard.UserInfo;
import com.osoeasy.models.Photo.PhotoList;
import com.osoeasy.models.Photo.Table1;

import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * Created by Sahil on 7/18/16.
 */
public class PhotoFragment extends BaseFragment implements CustomListAdapterInterface{

    private GridView gridView;
    List<Table1> table;
    private CustomListAdapter adapter;
     @Override
    public void initViews() {

        gridView = (GridView)findView(R.id.gv_photo);

        getData();
    }

    public void getData(){
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.PHOTO_LIST);
        object.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        object.setClassType(PhotoList.class);
        executeTask(AppConstants.TASK_CODES.PHOTO, object);

    }

    public void setData() {
        adapter = new CustomListAdapter(getActivity(),R.layout.row_photo,table,this);
        gridView.setAdapter(adapter);
    }
    @Override
    public int getViewID() {
        return R.layout.fragment_photo;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);

        if(response != null && ((PhotoList)response).getData().getTable1() != null){
            table = ((PhotoList)response).getData().getTable1();
            setData();
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.imageId.setText(table.get(position).getImage_id());
        return convertView;
    }

    class Holder {
        CustomFontTextView imageId;

        public Holder(View view) {
           imageId = (CustomFontTextView)view.findViewById(R.id.tv_photo);
        }
    }


}
