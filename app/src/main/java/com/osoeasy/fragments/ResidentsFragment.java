package com.osoeasy.fragments;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.Resident.ResidentData;
import com.osoeasy.models.Resident.Table1;

import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Sahil on 7/4/16.
 */
public class ResidentsFragment extends BaseFragment implements simplifii.framework.ListAdapters.CustomListAdapterInterface {

    ResidentData response;
    CustomListAdapter adapter;
    List<Table1> table;

    @Override
    public void initViews() {

        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.BASE_URL);
        object.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        object.addParameter("tblname", AppConstants.TABLES_NAME.RESIDENT);
        object.addParameter("cond", "");
        object.setClassType(ResidentData.class);
        executeTask(AppConstants.TASK_CODES.RESIDENT, object);


    }

    public void setdata() {
        ListView listView = (ListView) findView(R.id.lv_residents);
        adapter = new CustomListAdapter(getActivity(), R.layout.fragment_row_resident, table, this);
        listView.setAdapter(adapter);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null) {
            this.response = (ResidentData) response;
            this.table = ((ResidentData) response).getData().getTable1();
            setdata();
        }


    }

    @Override
    public int getViewID() {
        return R.layout.fragment_residents;
    }


    @Override
    public View getView(int position, View v, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (v == null) {
            v = inflater.inflate(resourceID, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.username.setText(table.get(position).getMb_name());
        holder.email.setText(table.get(position).getMb_email());
        holder.mobile.setText(table.get(position).getMb_regmobileno());
        holder.flat_details.setText(table.get(position).getMb_idproof());

        return v;
    }

    private class ViewHolder {
        private TextView username, email, mobile, flat_details;

        ViewHolder(View v) {
            username = (TextView) v.findViewById(R.id.tv_member_name);
            email = (TextView) v.findViewById(R.id.tv_email);
            mobile = (TextView) v.findViewById(R.id.tv_mobile);
            flat_details = (TextView) v.findViewById(R.id.tv_flat_details);
        }
    }
}
