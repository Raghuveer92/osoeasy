package com.osoeasy.fragments;

import com.osoeasy.R;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Sahil on 7/4/16.
 */
public class CommunicationGalleryFragment extends BaseFragment {
    @Override
    public void initViews() {

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_gallery;
    }
}
