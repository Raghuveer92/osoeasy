package com.osoeasy.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.osoeasy.activities.LoginActivity;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by nbansal2211 on 04/08/16.
 */
public class FragmentFactory {

    public static Fragment getFragment(int type) {
        Fragment f = null;
        switch (type) {
            case AppConstants.FRAGMENT_TYPE.HOME_FRAGMENT:
                break;
            case AppConstants.FRAGMENT_TYPE.CALENDAR:
                f = new CalenderFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.COMM_GALLERY:
                f = new CommunicationGalleryFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.COMPLAINTS:
                f = new ComplainsFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.DIRECTORY:
                f = new DirectoryFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.EASY_POLL:
                f = new EasyPollFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.FAMILY:
                f = new FamilyFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.INVITATION:
                f = new InvitationFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.MAINTAINENCE:
                f = new MaintenanceFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.NOTICE_BOARD:
                f = new NoticeBoardFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.PHOTOS:
                f = new PhotoFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.VIDEO:
                 f = new VideoFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.RESIDENTS:
                f = new ResidentTabFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.PARKING:
                f = new ParkingAllotmentFragment();
                break;


        }
        return f;
    }


}
