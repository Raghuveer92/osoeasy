package com.osoeasy.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.FamilyMembers.FamilyMember;
import com.osoeasy.models.FamilyMembers.Table1;

import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Sahil on 7/6/16.
 */
public class FamilyFragment extends BaseFragment implements CustomListAdapterInterface {

    FamilyMember response;
    CustomListAdapter adapter;
    List<Table1> table;
    @Override
    public void initViews() {

        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.BASE_URL);
        object.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        object.addParameter("tblname",AppConstants.TABLES_NAME.FAMILY_MEMBERS);
        object.addParameter("cond", "");
        object.setClassType(FamilyMember.class);
        executeTask(AppConstants.TASK_CODES.FAMILY_MEMBER, object);

    }

    public void setdata(){
        ListView listView = (ListView)findView(R.id.lv_family_members);
        adapter = new CustomListAdapter(getActivity(),R.layout.fragment_row_familymembers,table ,this);
        listView.setAdapter(adapter);
    }
    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response != null){
            this.response = (FamilyMember)response;
            this.table = ((FamilyMember) response).getData().getTable1();
            setdata();
        }



    }

    @Override
    public int getViewID() {
        return R.layout.fragment_family_members;
    }



    @Override
    public View getView(int position, View v, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if(v == null){
            v = inflater.inflate(resourceID, parent,false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        }else{
            holder = (ViewHolder) v.getTag();
        }

        holder.username.setText(table.get(position).getFm_fmname());
        holder.email.setText(table.get(position).getFm_emailid());
        holder.mobile.setText(table.get(position).getFm_mobileno());
        holder.flat_details.setText(table.get(position).getFm_flatno());

        return v;
    }

    private class ViewHolder{
        private TextView username, email, mobile, flat_details;
        ViewHolder(View v){
            username = (TextView)v.findViewById(R.id.tv_member_name);
            email = (TextView)v.findViewById(R.id.tv_email);
            mobile = (TextView)v.findViewById(R.id.tv_mobile);
            flat_details = (TextView)v.findViewById(R.id.tv_flat_details);
        }
    }
}
