package com.osoeasy.fragments;

import com.osoeasy.R;
import com.osoeasy.models.CommitteeStaff.CommitteeData;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by RAHU on 11-07-2016.
 */
public class CommitteeStaffFragment extends BaseFragment {

    @Override
    public void initViews() {
     getData();
    }

    private void getData() {
        HttpParamObject httpParamObject=new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.COMMITTEE_STAFF);
        httpParamObject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        httpParamObject.setClassType(CommitteeData.class);
        executeTask(AppConstants.TASK_CODES.COMMITTEESTAFF,httpParamObject);
    }
    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        CommitteeData commmitteedata = (CommitteeData) response;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_committee_staff;
    }
}
