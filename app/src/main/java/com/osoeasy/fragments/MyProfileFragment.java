package com.osoeasy.fragments;

import android.text.TextUtils;

import com.osoeasy.R;
import com.osoeasy.models.Calender.CalenderData;
import com.osoeasy.models.UserProfile.UserData;
import com.osoeasy.models.UserProfile.UserProfile;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Admin on 05-Aug-16.
 */
public class MyProfileFragment extends BaseFragment {
    @Override
    public void initViews() {
        getData();
    }

    private void getData() {
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.USER_PROFILE);
        object.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        object.setClassType(UserProfile.class);
        executeTask(AppConstants.TASK_CODES.USER_PROFILE, object);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_my_profile;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null && taskCode == AppConstants.TASK_CODES.USER_PROFILE) {
            UserProfile userProfile = (UserProfile) response;
            if (userProfile.getData().getUserData().size() > 0) {
                final UserData userData = userProfile.getData().getUserData().get(0);
                if (userData != null)
                    setData(userData);
            }
        }
    }

    private void setData(UserData userData) {
        setText(R.id.tv_name,userData.getMembersName());
        setText(R.id.tv_email,userData.getEmergAddress());
        setText(R.id.tv_member_id,userData.getMemberCode());
        setText(R.id.tv_username,userData.getUserName());
        setText(R.id.tv_society_id,userData.getSocietyId());
        setText(R.id.tv_father_name,userData.getFatherName());
        setText(R.id.tv_id_proof,userData.getIdProof());
        setText(R.id.tv_id_proof_number,userData.getIdProofNo());
        setText(R.id.tv_pay_mode,userData.getPayMode());
        setText(R.id.tv_pay_ammount,String.valueOf(userData.getPayAmount()));
        setText(R.id.tv_renewal_date,userData.getRenewalDate());
    }

    @Override
    protected void setText(int textViewID, String text) {
        if(TextUtils.isEmpty(text)){
            text="N/A";
        }
        super.setText(textViewID, text);
    }
}
