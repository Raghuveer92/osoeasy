package com.osoeasy.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.osoeasy.R;
import com.osoeasy.models.Calender.Table1;

import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.Util;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * Created by Sahil on 7/7/16.
 */
public class EventsListFragment extends BaseFragment implements CustomListAdapterInterface{

    List<Table1> table;
    CustomListAdapter adapter;
    ListView events;
    @Override
    public void initViews() {
        events = (ListView)findView(R.id.lv_events);
        adapter = new CustomListAdapter(getActivity(),R.layout.row_event_list,table,this);
        events.setAdapter(adapter);

    }

    public static EventsListFragment getInstance(List<Table1> table)
    {
        EventsListFragment eventsListFragment = new EventsListFragment();
        eventsListFragment.table = table;

        return eventsListFragment;
     }
    @Override
    public int getViewID() {
        return R.layout.fragment_event_list_row;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if(v == null){
            v = inflater.inflate(resourceID, null);
            holder = new ViewHolder(v);
            v.setTag(holder);
        }else{
            holder = (ViewHolder) v.getTag();
        }

        String date = table.get(position).getCa_sdate();
        String month_date_pattern = Util.convertDateFormat(date, Util.DISCVER_SERVER_DATE_PATTERN,Util.MONTH_DAY_YEAR_DATE_PATTERN );
        holder.event_title.setText(table.get(position).getCa_title());
        holder.event_date.setText(month_date_pattern);

        return v;
    }

    private class ViewHolder{
        private CustomFontTextView event_title,event_date;
        ViewHolder(View v){
            event_title = (CustomFontTextView)v.findViewById(R.id.tv_event_title);
            event_date = (CustomFontTextView)v.findViewById(R.id.tv_event_date);
        }
    }

}
