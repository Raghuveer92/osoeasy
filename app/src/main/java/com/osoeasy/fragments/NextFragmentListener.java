package com.osoeasy.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by Sahil on 7/4/16.
 */
public interface NextFragmentListener {
    void startNext(Fragment fragment);
}
