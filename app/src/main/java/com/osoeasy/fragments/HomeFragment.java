package com.osoeasy.fragments;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.home.DashBoardOther;
import com.osoeasy.models.home.HomeResponce;
import com.osoeasy.models.home.Timeline;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * Created by Admin on 14-Jun-16.
 */
public class HomeFragment extends BaseFragment implements CustomListAdapterInterface {
    ListView listView;
    List<Timeline> timelineList = new ArrayList<>();
    private CustomListAdapter listAdapter;
    private HomeResponce response;

    CustomFontTextView TotAnnouncement,TotComplainPending, TotMembers ,TotPendingAmount, TotPendingApproval, TotPendingCount, TotPoll;
    public static HomeFragment getInstance() {
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }

    private void getDashBoardData() {
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.DASH_BOARD_URL);
        object.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        object.setClassType(HomeResponce.class);
        executeTask(AppConstants.TASK_CODES.HOME, object);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response != null){
            String str = response.toString();
            Log.d("response", str);
            this.response = (HomeResponce) response;
            this.timelineList = ((HomeResponce) response).getData().getTimeline();
            setData();
        }
    }

    @Override
    public void initViews() {
        getDashBoardData();
    }

    private void setData(){
        View headerView = LayoutInflater.from(getActivity()).inflate(R.layout.view_header, null, false);
        listView = (ListView) findView(R.id.list_home);
        listView.addHeaderView(headerView);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_home_list, timelineList, this);
        listView.setAdapter(listAdapter);
        initHeaders(headerView);
    }

    private void initHeaders(View headerView) {
        List<DashBoardOther> others = response.getData().getOther();
        String str = others.toString();
        Log.d("response  ++",str);
        HashMap hashmap = new HashMap<String,Integer>();
        for(int i=0;i<others.size();i++)
        {
            DashBoardOther dashBoardOther = others.get(i);

            if(dashBoardOther.getTotAnnouncement() != null)
            hashmap.put("TotAnnouncement",dashBoardOther.getTotAnnouncement());
            if(dashBoardOther.getTotComplainPending()  != null)
            hashmap.put("TotComplainPending",dashBoardOther.getTotComplainPending());
            if(dashBoardOther.getTotMember() != null)
            hashmap.put("TotMembers",dashBoardOther.getTotMember());
            if(dashBoardOther.getTotPendingAmount()  != null)
            hashmap.put("TotPendingAmount", dashBoardOther.getTotPendingAmount());
            if(dashBoardOther.getTotPendingApproval()  != null)
            hashmap.put("TotPendingApproval",dashBoardOther.getTotPendingApproval());
            if(dashBoardOther.getTotPendingCount()!= null)
            hashmap.put("TotPendingCount",dashBoardOther.getTotPendingCount());
            if(dashBoardOther.getTotPoll() != null)
            hashmap.put("TotPoll", dashBoardOther.getTotPoll());

        }

        TotAnnouncement = (CustomFontTextView)findView(R.id.tv_announcements);
        TotComplainPending = (CustomFontTextView)findView(R.id.tv_pendingComplaints);
        TotMembers = (CustomFontTextView)findView(R.id.tv_residents);
        TotPendingAmount = (CustomFontTextView)findView(R.id.tv_pendingAmount);
        TotPendingApproval = (CustomFontTextView)findView(R.id.tv_pendingApprovals);
        TotPendingCount = (CustomFontTextView)findView(R.id.tv_pendingCount);
        TotPoll = (CustomFontTextView)findView(R.id.tv_easypoll);

        TotAnnouncement.setText(""+hashmap.get("TotAnnouncement"));
        TotComplainPending.setText(""+hashmap.get("TotComplainPending"));
        TotMembers.setText(""+hashmap.get("TotMembers"));
        TotPendingAmount.setText(""+hashmap.get("TotPendingAmount"));
        TotPendingApproval.setText(""+hashmap.get("TotPendingApproval"));
        TotPendingCount.setText(""+hashmap.get("TotPendingCount"));
        TotPoll.setText(""+hashmap.get("TotPoll"));
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_home;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent,false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final Timeline timeline = timelineList.get(position);
        holder.tvTitle.setText(timeline.getnTitle());
        holder.tvSubTitle.setText(Html.fromHtml(timeline.getnType() + " " + timeline.getnDate()));
        holder.tvDetail.setText(timeline.getnDetails());
        return convertView;
    }

    class Holder {
        TextView tvTitle, tvSubTitle, tvDetail;

        public Holder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tv_row_home_title);
            tvSubTitle = (TextView) view.findViewById(R.id.tv_row_home_subtitle);
            tvDetail = (TextView) view.findViewById(R.id.tv_row_home_detail);
        }
    }
}
