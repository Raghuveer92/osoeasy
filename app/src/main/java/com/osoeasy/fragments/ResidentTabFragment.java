package com.osoeasy.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import com.osoeasy.R;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by nbansal2211 on 12/07/16.
 */
public class ResidentTabFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface, ViewPager.OnPageChangeListener{

    List<Fragment> fragmentList;
    List<String> list;
    CustomPagerAdapter adapter;

    @Override
    public void initViews() {


        list = new ArrayList<>();
        fragmentList  = new ArrayList<>();
        ViewPager viewPager = (ViewPager)findView(R.id.vp_directory);
        list.add("Family");
        list.add("Tenants");
        list.add("Servant/Driver");

        adapter = new CustomPagerAdapter(getChildFragmentManager(),list,this);
        viewPager.setAdapter(adapter);
        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip)findView(R.id.tabs);
        tabStrip.setViewPager(viewPager);

        FamilyFragment familyFragment = new FamilyFragment();
        fragmentList.add(familyFragment);

        Tenants tenants = new Tenants();
        fragmentList.add(tenants);

        ServentDriverFragment serventDriverFragment = new ServentDriverFragment();
        fragmentList.add(serventDriverFragment);

    }


    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return fragmentList.get(position);
    }
    @Override
    public int getViewID() {
        return R.layout.fragment_directory;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return list.get(position);
    }
}