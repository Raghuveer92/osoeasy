package com.osoeasy.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.BaseApi;
import com.osoeasy.models.Invitation.UserData;
import com.osoeasy.models.Invitation.UserInfo;
import com.osoeasy.models.Invitation.table1;


import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * Created by Sahil on 7/4/16.
 */
public class InvitationFragment extends BaseFragment implements CustomListAdapterInterface {

    private List<UserInfo> table1;
    private ListView listView;
    private CustomListAdapter adapter;

    @Override
    public void initViews() {
        getData();
    }

    private void getData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.INVITATION_LIST);
        httpParamObject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        httpParamObject.setClassType(UserData.class);
        executeTask(AppConstants.TASK_CODES.INVITATION, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null) {
            table1 = ((UserData) response).getData().getUserInfoList();
            if (table1 != null)
                setData();
        }
    }

    public void setData() {
        listView = (ListView) findView(R.id.lv_invitation);
        adapter = new CustomListAdapter(getActivity(), R.layout.row_invitation, table1, this);
        listView.setAdapter(adapter);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_invitation;
    }


    @Override
    public View getView(int position, View v, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        ViewHolder holder = null;
        if (v == null) {
            v = inflater.inflate(resourceID, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.invitationTitle.setText("Invitation For " + table1.get(position).getIn_title());
        holder.name.setText(table1.get(position).getIn_fmbid());
        String date = getDate(table1.get(position).getLDate());
        holder.date.setText(date);
        return v;
    }

    public String getDate(String date) {
        String Date = Util.convertDateFormat(date, Util.SLASH_PATTERN_DATE, Util.MONTH_DAY_YEAR_DATE_PATTERN);
        return Date;
    }

    private class ViewHolder {
        private CustomFontTextView invitationTitle, name, date;

        ViewHolder(View v) {
            invitationTitle = (CustomFontTextView) v.findViewById(R.id.tv_invitation_title);
            name = (CustomFontTextView) v.findViewById(R.id.tv_name);
            date = (CustomFontTextView) v.findViewById(R.id.tv_date);
        }


    }
}
