package com.osoeasy.models.UserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 03-08-2016.
 */
public class UserData {
    @SerializedName("MemberCode")
    @Expose
    public String memberCode;
    @SerializedName("UserName")
    @Expose
    public String userName;
    @SerializedName("MembersName")
    @Expose
    public String membersName;
    @SerializedName("SocietyId")
    @Expose
    public String societyId;
    @SerializedName("FatherName")
    @Expose
    public String fatherName;
    @SerializedName("idProof")
    @Expose
    public String idProof;
    @SerializedName("idProofNo")
    @Expose
    public String idProofNo;
    @SerializedName("EmergAddress")
    @Expose
    public String emergAddress;
    @SerializedName("PayType")
    @Expose
    public String payType;
    @SerializedName("PayMode")
    @Expose
    public String payMode;
    @SerializedName("PayAmount")
    @Expose
    public Double payAmount;
    @SerializedName("isIndian")
    @Expose
    public Boolean isIndian;
    @SerializedName("isForeign")
    @Expose
    public Boolean isForeign;
    @SerializedName("isPhycially")
    @Expose
    public Boolean isPhycially;
    @SerializedName("isFreedomF")
    @Expose
    public Boolean isFreedomF;
    @SerializedName("isExserviceMan")
    @Expose
    public Boolean isExserviceMan;
    @SerializedName("isSocialActivities")
    @Expose
    public Boolean isSocialActivities;
    @SerializedName("RenewalDate")
    @Expose
    public String renewalDate;

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMembersName() {
        return membersName;
    }

    public void setMembersName(String membersName) {
        this.membersName = membersName;
    }

    public String getSocietyId() {
        return societyId;
    }

    public void setSocietyId(String societyId) {
        this.societyId = societyId;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getIdProof() {
        return idProof;
    }

    public void setIdProof(String idProof) {
        this.idProof = idProof;
    }

    public String getIdProofNo() {
        return idProofNo;
    }

    public void setIdProofNo(String idProofNo) {
        this.idProofNo = idProofNo;
    }

    public String getEmergAddress() {
        return emergAddress;
    }

    public void setEmergAddress(String emergAddress) {
        this.emergAddress = emergAddress;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public Double getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Double payAmount) {
        this.payAmount = payAmount;
    }

    public Boolean getIndian() {
        return isIndian;
    }

    public void setIndian(Boolean indian) {
        isIndian = indian;
    }

    public Boolean getForeign() {
        return isForeign;
    }

    public void setForeign(Boolean foreign) {
        isForeign = foreign;
    }

    public Boolean getPhycially() {
        return isPhycially;
    }

    public void setPhycially(Boolean phycially) {
        isPhycially = phycially;
    }

    public Boolean getFreedomF() {
        return isFreedomF;
    }

    public void setFreedomF(Boolean freedomF) {
        isFreedomF = freedomF;
    }

    public Boolean getExserviceMan() {
        return isExserviceMan;
    }

    public void setExserviceMan(Boolean exserviceMan) {
        isExserviceMan = exserviceMan;
    }

    public Boolean getSocialActivities() {
        return isSocialActivities;
    }

    public void setSocialActivities(Boolean socialActivities) {
        isSocialActivities = socialActivities;
    }

    public String getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(String renewalDate) {
        this.renewalDate = renewalDate;
    }
}
