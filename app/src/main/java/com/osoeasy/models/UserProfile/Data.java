package com.osoeasy.models.UserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAHU on 03-08-2016.
 */

public class Data {
    @SerializedName("table1")
    @Expose
    public List<UserData> userData = new ArrayList<UserData>();

    public List<UserData> getUserData() {
        return userData;
    }

    public void setUserData(List<UserData> userData) {
        this.userData = userData;
    }
}
