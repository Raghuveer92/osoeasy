package com.osoeasy.models.ParkingAllotment;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 11-07-2016.
 */
public class ParkingInfo {
    @SerializedName("StaffCode")
    String staffCode;

    public String getStaffCode() {
        return staffCode;
    }

    public String getStaffName() {
        return staffName;
    }

    public String getProfile() {
        return profile;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getPoliceVerno() {
        return policeVerno;
    }

    public String getEmployer() {
        return employer;
    }

    public String getEmployerPhone() {
        return employerPhone;
    }

    public String getSocietyId() {
        return societyId;
    }

    @SerializedName("StaffName")
    String staffName;

    @SerializedName("Profile")
    String profile;

    @SerializedName("PhoneNo")
    String phoneNo;

    @SerializedName("PoliceVerNo")
    String policeVerno;

    @SerializedName("Employer")
    String employer;

    @SerializedName("EmployerPhone")
    String employerPhone;

    @SerializedName("SocietyID")
    String societyId;
}
