package com.osoeasy.models.ParkingAllotment;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RAHU on 11-07-2016.
 */
public class Parkingtable1 {
   @SerializedName("table1")
    List<ParkingInfo> parkingInfoList;

    public List<ParkingInfo> getParkingInfoList() {
        return parkingInfoList;
    }
}
