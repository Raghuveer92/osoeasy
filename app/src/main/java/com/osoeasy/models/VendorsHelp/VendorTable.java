package com.osoeasy.models.VendorsHelp;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RAHU on 11-07-2016.
 */
public class VendorTable {
    @SerializedName("table1")
    List<VendorInfo> vendorInfoList;

    public List<VendorInfo> getVendorInfoList() {
        return vendorInfoList;
    }
}
