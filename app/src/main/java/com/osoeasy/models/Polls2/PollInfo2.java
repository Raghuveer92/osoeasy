package com.osoeasy.models.Polls2;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 11-07-2016.
 */
public class PollInfo2 {

    @SerializedName("Topic")
    String topic;

    @SerializedName("TotalCount")
    long totalCount;

    @SerializedName("Opinion")
    String opinion;

    @SerializedName("Counter")
    long counter;

    @SerializedName("PollKey")
    String pollKey;

    @SerializedName("oq_opid")
    String oq_opid;

    @SerializedName("AvgCounter")
    long avgCounter;

    public String getTopic() {
        return topic;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public String getOpinion() {
        return opinion;
    }

    public long getCounter() {
        return counter;
    }

    public String getPollKey() {
        return pollKey;
    }

    public String getOq_opid() {
        return oq_opid;
    }

    public long getAvgCounter() {
        return avgCounter;
    }
}
