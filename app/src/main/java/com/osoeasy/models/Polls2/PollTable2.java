package com.osoeasy.models.Polls2;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RAHU on 11-07-2016.
 */
public class PollTable2 {
    @SerializedName("table1")
    List<PollInfo2> pollInfo2List;

    public List<PollInfo2> getPollInfo2List() {
        return pollInfo2List;
    }
}
