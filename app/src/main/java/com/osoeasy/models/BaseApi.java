package com.osoeasy.models;

/**
 * Created by Admin on 13-Jun-16.
 */
public class BaseApi {
    String message;
    boolean error;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
