package com.osoeasy.models.CompTypeDropDownList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 03-08-2016.
 */
public class Table1 {
    @SerializedName("Code")
    @Expose
    public String code;
    @SerializedName("Description")
    @Expose
    public String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
