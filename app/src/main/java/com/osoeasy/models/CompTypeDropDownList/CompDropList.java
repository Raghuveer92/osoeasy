package com.osoeasy.models.CompTypeDropDownList;

import com.osoeasy.models.BaseApi;

/**
 * Created by RAHU on 04-08-2016.
 */
public class CompDropList extends BaseApi {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
