package com.osoeasy.models.NoticeBoard;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 11-07-2016.
 */
public class UserInfo {

    @SerializedName("nType")
    String nType;

    @SerializedName("nDate")
    String nDate;

    @SerializedName("nSender")
    String nSender;

    @SerializedName("nTitle")
    String ntitle;

    @SerializedName("nDetails")
    String nDetails;

    @SerializedName("nSrlNo")
    String nSrlNo;

    @SerializedName("nInActive")
    String nInActive;

    public String getnType() {
        return nType;
    }

    public String getnDate() {
        return nDate;
    }

    public String getnSender() {
        return nSender;
    }

    public String getNtitle() {
        return ntitle;
    }

    public String getnDetails() {
        return nDetails;
    }

    public String getnSrlNo() {
        return nSrlNo;
    }

    public String getnInActive() {
        return nInActive;
    }
}
