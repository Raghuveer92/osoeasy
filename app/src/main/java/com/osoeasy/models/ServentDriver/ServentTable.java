package com.osoeasy.models.ServentDriver;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RAHU on 11-07-2016.
 */
public class ServentTable {
    @SerializedName("table1")
    List<ServentInfo> serventInfoList;

    public List<ServentInfo> getServentInfoList() {
        return serventInfoList;
    }
}
