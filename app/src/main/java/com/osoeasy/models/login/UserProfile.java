package com.osoeasy.models.login;

/**
 * Created by Admin on 13-Jun-16.
 */
public class UserProfile {
    String name;
    String email;
    String role;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
