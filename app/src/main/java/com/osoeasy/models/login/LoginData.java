package com.osoeasy.models.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 13-Jun-16.
 */
public class LoginData {
    @SerializedName("userprofile")
    UserProfile userProfile;

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}
