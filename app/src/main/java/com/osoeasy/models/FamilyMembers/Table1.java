package com.osoeasy.models.FamilyMembers;

/**
 * Created by Sahil on 7/6/16.
 */

        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;


public class Table1 {

    @SerializedName("fm_fmid")
    @Expose
    private String fm_fmid;
    @SerializedName("fm_fmname")
    @Expose
    private String fm_fmname;
    @SerializedName("fm_mobileno")
    @Expose
    private String fm_mobileno;
    @SerializedName("fm_emailid")
    @Expose
    private String fm_emailid;
    @SerializedName("fm_dob")
    @Expose
    private String fm_dob;
    @SerializedName("fm_flatno")
    @Expose
    private String fm_flatno;
    @SerializedName("fm_relationship")
    @Expose
    private String fm_relationship;
    @SerializedName("fm_relationshipoth")
    @Expose
    private String fm_relationshipoth;
    @SerializedName("fm_rmrks")
    @Expose
    private Object fm_rmrks;
    @SerializedName("fm_active")
    @Expose
    private Boolean fm_active;
    @SerializedName("fm_edtm")
    @Expose
    private String fm_edtm;
    @SerializedName("fm_eby")
    @Expose
    private String fm_eby;
    @SerializedName("fm_mdtm")
    @Expose
    private Object fm_mdtm;
    @SerializedName("fm_mby")
    @Expose
    private Object fm_mby;
    @SerializedName("fm_cdtm")
    @Expose
    private Object fm_cdtm;
    @SerializedName("fm_cby")
    @Expose
    private Object fm_cby;
    @SerializedName("fm_coid")
    @Expose
    private String fm_coid;
    @SerializedName("fm_type")
    @Expose
    private String fm_type;
    @SerializedName("fm_mbid")
    @Expose
    private String fm_mbid;

    /**
     *
     * @return
     * The fm_fmid
     */
    public String getFm_fmid() {
        return fm_fmid;
    }

    /**
     *
     * @param fm_fmid
     * The fm_fmid
     */
    public void setFm_fmid(String fm_fmid) {
        this.fm_fmid = fm_fmid;
    }

    /**
     *
     * @return
     * The fm_fmname
     */
    public String getFm_fmname() {
        return fm_fmname;
    }

    /**
     *
     * @param fm_fmname
     * The fm_fmname
     */
    public void setFm_fmname(String fm_fmname) {
        this.fm_fmname = fm_fmname;
    }

    /**
     *
     * @return
     * The fm_mobileno
     */
    public String getFm_mobileno() {
        return fm_mobileno;
    }

    /**
     *
     * @param fm_mobileno
     * The fm_mobileno
     */
    public void setFm_mobileno(String fm_mobileno) {
        this.fm_mobileno = fm_mobileno;
    }

    /**
     *
     * @return
     * The fm_emailid
     */
    public String getFm_emailid() {
        return fm_emailid;
    }

    /**
     *
     * @param fm_emailid
     * The fm_emailid
     */
    public void setFm_emailid(String fm_emailid) {
        this.fm_emailid = fm_emailid;
    }

    /**
     *
     * @return
     * The fm_dob
     */
    public String getFm_dob() {
        return fm_dob;
    }

    /**
     *
     * @param fm_dob
     * The fm_dob
     */
    public void setFm_dob(String fm_dob) {
        this.fm_dob = fm_dob;
    }

    /**
     *
     * @return
     * The fm_flatno
     */
    public String getFm_flatno() {
        return fm_flatno;
    }

    /**
     *
     * @param fm_flatno
     * The fm_flatno
     */
    public void setFm_flatno(String fm_flatno) {
        this.fm_flatno = fm_flatno;
    }

    /**
     *
     * @return
     * The fm_relationship
     */
    public String getFm_relationship() {
        return fm_relationship;
    }

    /**
     *
     * @param fm_relationship
     * The fm_relationship
     */
    public void setFm_relationship(String fm_relationship) {
        this.fm_relationship = fm_relationship;
    }

    /**
     *
     * @return
     * The fm_relationshipoth
     */
    public String getFm_relationshipoth() {
        return fm_relationshipoth;
    }

    /**
     *
     * @param fm_relationshipoth
     * The fm_relationshipoth
     */
    public void setFm_relationshipoth(String fm_relationshipoth) {
        this.fm_relationshipoth = fm_relationshipoth;
    }

    /**
     *
     * @return
     * The fm_rmrks
     */
    public Object getFm_rmrks() {
        return fm_rmrks;
    }

    /**
     *
     * @param fm_rmrks
     * The fm_rmrks
     */
    public void setFm_rmrks(Object fm_rmrks) {
        this.fm_rmrks = fm_rmrks;
    }

    /**
     *
     * @return
     * The fm_active
     */
    public Boolean getFm_active() {
        return fm_active;
    }

    /**
     *
     * @param fm_active
     * The fm_active
     */
    public void setFm_active(Boolean fm_active) {
        this.fm_active = fm_active;
    }

    /**
     *
     * @return
     * The fm_edtm
     */
    public String getFm_edtm() {
        return fm_edtm;
    }

    /**
     *
     * @param fm_edtm
     * The fm_edtm
     */
    public void setFm_edtm(String fm_edtm) {
        this.fm_edtm = fm_edtm;
    }

    /**
     *
     * @return
     * The fm_eby
     */
    public String getFm_eby() {
        return fm_eby;
    }

    /**
     *
     * @param fm_eby
     * The fm_eby
     */
    public void setFm_eby(String fm_eby) {
        this.fm_eby = fm_eby;
    }

    /**
     *
     * @return
     * The fm_mdtm
     */
    public Object getFm_mdtm() {
        return fm_mdtm;
    }

    /**
     *
     * @param fm_mdtm
     * The fm_mdtm
     */
    public void setFm_mdtm(Object fm_mdtm) {
        this.fm_mdtm = fm_mdtm;
    }

    /**
     *
     * @return
     * The fm_mby
     */
    public Object getFm_mby() {
        return fm_mby;
    }

    /**
     *
     * @param fm_mby
     * The fm_mby
     */
    public void setFm_mby(Object fm_mby) {
        this.fm_mby = fm_mby;
    }

    /**
     *
     * @return
     * The fm_cdtm
     */
    public Object getFm_cdtm() {
        return fm_cdtm;
    }

    /**
     *
     * @param fm_cdtm
     * The fm_cdtm
     */
    public void setFm_cdtm(Object fm_cdtm) {
        this.fm_cdtm = fm_cdtm;
    }

    /**
     *
     * @return
     * The fm_cby
     */
    public Object getFm_cby() {
        return fm_cby;
    }

    /**
     *
     * @param fm_cby
     * The fm_cby
     */
    public void setFm_cby(Object fm_cby) {
        this.fm_cby = fm_cby;
    }

    /**
     *
     * @return
     * The fm_coid
     */
    public String getFm_coid() {
        return fm_coid;
    }

    /**
     *
     * @param fm_coid
     * The fm_coid
     */
    public void setFm_coid(String fm_coid) {
        this.fm_coid = fm_coid;
    }

    /**
     *
     * @return
     * The fm_type
     */
    public String getFm_type() {
        return fm_type;
    }

    /**
     *
     * @param fm_type
     * The fm_type
     */
    public void setFm_type(String fm_type) {
        this.fm_type = fm_type;
    }

    /**
     *
     * @return
     * The fm_mbid
     */
    public String getFm_mbid() {
        return fm_mbid;
    }

    /**
     *
     * @param fm_mbid
     * The fm_mbid
     */
    public void setFm_mbid(String fm_mbid) {
        this.fm_mbid = fm_mbid;
    }

}