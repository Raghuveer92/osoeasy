package com.osoeasy.models.CompLogEntry;

import com.osoeasy.models.BaseApi;

/**
 * Created by RAHU on 04-08-2016.
 */
public class CompLogEntry extends BaseApi {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
