package com.osoeasy.models.emergency;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.osoeasy.models.BaseApi;

import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

/**
 * Created by nbansal2211 on 04/08/16.
 */
public class EmergencyListResponse extends BaseApi{

    @SerializedName("data")
    @Expose
    private Data data;

    /**
     * @return The data
     */
    public Data getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(Data data) {
        this.data = data;
    }

    public static EmergencyListResponse getInstance(){
        String json = Preferences.getData(Preferences.EMERGENCY_LIST, "");
        if(!TextUtils.isEmpty(json)){
            return parseJson(json);
        }
        return null;
    }
    public static EmergencyListResponse parseJson(String json){
        EmergencyListResponse user = (EmergencyListResponse) JsonUtil.parseJson(json, EmergencyListResponse.class);
        if(user != null && !user.isError() && user.getData() != null && user.getData().getTable1() != null && user.getData().getTable1().size()>0){
            Preferences.saveData(Preferences.EMERGENCY_LIST, json);
        }
        return user;
    }

}
