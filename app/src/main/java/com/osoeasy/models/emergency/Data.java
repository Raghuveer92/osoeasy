package com.osoeasy.models.emergency;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Data {

    @SerializedName("table1")
    @Expose
    private List<EmergencyMember> table1 = new ArrayList<EmergencyMember>();

    /**
     * @return The userData
     */
    public List<EmergencyMember> getTable1() {
        return table1;
    }

    /**
     * @param table1 The userData
     */
    public void setTable1(List<EmergencyMember> table1) {
        this.table1 = table1;
    }

}

