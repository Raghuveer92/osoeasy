package com.osoeasy.models.emergency;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nbansal2211 on 04/08/16.
 */
public class EmergencyMember {

    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("RelationShip")
    @Expose
    private String relationShip;

    /**
     * @return The mobileNo
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * @param mobileNo The MobileNo
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The relationShip
     */
    public String getRelationShip() {
        return relationShip;
    }

    /**
     * @param relationShip The RelationShip
     */
    public void setRelationShip(String relationShip) {
        this.relationShip = relationShip;
    }

}
