package com.osoeasy.models.Tenants;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RAHU on 11-07-2016.
 */
public class TenantTable {
    @SerializedName("table1")
    List<TenantInfo> tenantInfoList;

    public List<TenantInfo> getTenantInfoList() {
        return tenantInfoList;
    }
}
