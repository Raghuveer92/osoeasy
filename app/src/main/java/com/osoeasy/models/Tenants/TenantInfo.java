package com.osoeasy.models.Tenants;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 11-07-2016.
 */
public class TenantInfo {
    @SerializedName("TenantCode")
    @Expose
    public String tenantCode;
    @SerializedName("TenantName")
    @Expose
    public String tenantName;
    @SerializedName("FlatDetails")
    @Expose
    public String flatDetails;
    @SerializedName("RegMobileNo")
    @Expose
    public String regMobileNo;
    @SerializedName("EmailID")
    @Expose
    public String emailID;
    @SerializedName("SocietyID")
    @Expose
    public String societyID;
    @SerializedName("MemberCode")
    @Expose
    public String memberCode;

    public String getTenantCode() {
        return tenantCode;
    }

    public String getTenantName() {
        return tenantName;
    }

    public String getFlatDetails() {
        return flatDetails;
    }

    public String getRegMobileNo() {
        return regMobileNo;
    }

    public String getEmailID() {
        return emailID;
    }

    public String getSocietyID() {
        return societyID;
    }

    public String getMemberCode() {
        return memberCode;
    }
}
