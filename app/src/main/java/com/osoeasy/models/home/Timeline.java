package com.osoeasy.models.home;

/**
 * Created by Admin on 13-Jun-16.
 */
public class Timeline {
    String nType;
    String nDate;
    String nSender;
    String nTitle;
    String nDetails;
    long nSrlNo;
    long nInActive;

    public String getnType() {
        return nType;
    }

    public void setnType(String nType) {
        this.nType = nType;
    }

    public String getnDate() {
        return nDate;
    }

    public void setnDate(String nDate) {
        this.nDate = nDate;
    }

    public String getnSender() {
        return nSender;
    }

    public void setnSender(String nSender) {
        this.nSender = nSender;
    }

    public String getnTitle() {
        return nTitle;
    }

    public void setnTitle(String nTitle) {
        this.nTitle = nTitle;
    }

    public String getnDetails() {
        return nDetails;
    }

    public void setnDetails(String nDetails) {
        this.nDetails = nDetails;
    }

    public long getnSrlNo() {
        return nSrlNo;
    }

    public void setnSrlNo(long nSrlNo) {
        this.nSrlNo = nSrlNo;
    }

    public long getnInActive() {
        return nInActive;
    }

    public void setnInActive(long nInActive) {
        this.nInActive = nInActive;
    }
}
