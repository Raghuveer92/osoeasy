package com.osoeasy.models.home;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 13-Jun-16.
 */
public class HomeData {
    List<FamilyDetail> familydetails = new ArrayList<>();
    List<Timeline> timeline = new ArrayList<>();
    List<DashBoardOther> other = new ArrayList<>();

    public List<FamilyDetail> getFamilydetails() {
        return familydetails;
    }

    public List<Timeline> getTimeline() {
        return timeline;
    }

    public List<DashBoardOther> getOther() {
        return other;
    }
}
