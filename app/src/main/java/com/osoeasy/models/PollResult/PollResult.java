package com.osoeasy.models.PollResult;

import com.osoeasy.models.BaseApi;

/**
 * Created by Sahil on 7/18/16.
 */

public class PollResult extends BaseApi{

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
