package com.osoeasy.models.PollResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sahil on 7/18/16.
 */
public class Table1 {
    @SerializedName("Topic")
    @Expose
    private String topic;
    @SerializedName("TotalCount")
    @Expose
    private Double totalCount;
    @SerializedName("Opinion")
    @Expose
    private String opinion;
    @SerializedName("Counter")
    @Expose
    private Double counter;
    @SerializedName("PollKey")
    @Expose
    private String pollKey;
    @SerializedName("oq_opid")
    @Expose
    private String oq_opid;
    @SerializedName("AvgCounter")
    @Expose
    private Double avgCounter;

    /**
     *
     * @return
     * The topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     *
     * @param topic
     * The Topic
     */
    public void setTopic(String topic) {
        this.topic = topic;
    }

    /**
     *
     * @return
     * The totalCount
     */
    public Double getTotalCount() {
        return totalCount;
    }

    /**
     *
     * @param totalCount
     * The TotalCount
     */
    public void setTotalCount(Double totalCount) {
        this.totalCount = totalCount;
    }

    /**
     *
     * @return
     * The opinion
     */
    public String getOpinion() {
        return opinion;
    }

    /**
     *
     * @param opinion
     * The Opinion
     */
    public void setOpinion(String opinion) {
        this.opinion = opinion;
    }

    /**
     *
     * @return
     * The counter
     */
    public Double getCounter() {
        return counter;
    }

    /**
     *
     * @param counter
     * The Counter
     */
    public void setCounter(Double counter) {
        this.counter = counter;
    }

    /**
     *
     * @return
     * The pollKey
     */
    public String getPollKey() {
        return pollKey;
    }

    /**
     *
     * @param pollKey
     * The PollKey
     */
    public void setPollKey(String pollKey) {
        this.pollKey = pollKey;
    }

    /**
     *
     * @return
     * The oq_opid
     */
    public String getOq_opid() {
        return oq_opid;
    }

    /**
     *
     * @param oq_opid
     * The oq_opid
     */
    public void setOq_opid(String oq_opid) {
        this.oq_opid = oq_opid;
    }

    /**
     *
     * @return
     * The avgCounter
     */
    public Double getAvgCounter() {
        return avgCounter;
    }

    /**
     *
     * @param avgCounter
     * The AvgCounter
     */
    public void setAvgCounter(Double avgCounter) {
        this.avgCounter = avgCounter;
    }
}
