package com.osoeasy.models.Photo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahil on 7/19/16.
 */
public class Data {

    @SerializedName("table1")
    @Expose

    List<Table1> table1 = new ArrayList<Table1>();

    public List<Table1> getTable1() {
        return table1;
    }

    public void setTable1(List<Table1> table1) {
        this.table1 = table1;
    }
}
