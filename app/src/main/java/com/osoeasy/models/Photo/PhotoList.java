package com.osoeasy.models.Photo;

import com.osoeasy.models.BaseApi;

/**
 * Created by Sahil on 7/19/16.
 */
public class PhotoList extends BaseApi{

    Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
