package com.osoeasy.models.Photo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sahil on 7/19/16.
 */
public class Table1 {

    @SerializedName("im_imid")
    @Expose
    private String image_id;
    @SerializedName("MyImage")
    @Expose
    private String image;

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
