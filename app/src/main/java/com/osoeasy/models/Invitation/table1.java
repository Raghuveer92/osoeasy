package com.osoeasy.models.Invitation;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RAHU on 11-07-2016.
 */
public class table1 {
    @SerializedName("table1")
    List<UserInfo> userInfoList;

    public List<UserInfo> getUserInfoList() {
        return userInfoList;
    }

    public void setUserInfoList(List<UserInfo> userInfoList) {
        this.userInfoList = userInfoList;
    }
}
