package com.osoeasy.models.ComplainStatusList;

import com.osoeasy.models.BaseApi;

/**
 * Created by RAHU on 03-08-2016.
 */
public class ComplaintResponse extends BaseApi {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
