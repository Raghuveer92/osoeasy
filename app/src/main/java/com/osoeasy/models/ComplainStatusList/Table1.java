package com.osoeasy.models.ComplainStatusList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 03-08-2016.
 */
public class Table1 {
    @SerializedName("SrlNo")
    @Expose
    public Integer srlNo;
    @SerializedName("TicketNo")
    @Expose
    public String ticketNo;
    @SerializedName("TicketDate")
    @Expose
    public String ticketDate;
    @SerializedName("MemberCode")
    @Expose
    public String memberCode;
    @SerializedName("Status")
    @Expose
    public String status;
    @SerializedName("MemberName")
    @Expose
    public String memberName;
    @SerializedName("TypeCode")
    @Expose
    public String typeCode;
    @SerializedName("TypeName")
    @Expose
    public String typeName;
    @SerializedName("Subject")
    @Expose
    public String subject;
    @SerializedName("Descrption")
    @Expose
    public String descrption;
    @SerializedName("LogBy")
    @Expose
    public String logBy;
    @SerializedName("SocietyName")
    @Expose
    public String societyName;

    public Integer getSrlNo() {
        return srlNo;
    }

    public void setSrlNo(Integer srlNo) {
        this.srlNo = srlNo;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getTicketDate() {
        return ticketDate;
    }

    public void setTicketDate(String ticketDate) {
        this.ticketDate = ticketDate;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }



    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescrption() {
        return descrption;
    }

    public void setDescrption(String descrption) {
        this.descrption = descrption;
    }

    public String getLogBy() {
        return logBy;
    }

    public void setLogBy(String logBy) {
        this.logBy = logBy;
    }

    public String getSocietyName() {
        return societyName;
    }

    public void setSocietyName(String societyName) {
        this.societyName = societyName;
    }
}
