package com.osoeasy.models.Resident;

import com.osoeasy.models.BaseApi;

/**
 * Created by sahil on 7/6/16.
 */

public class ResidentData extends BaseApi {

    Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
