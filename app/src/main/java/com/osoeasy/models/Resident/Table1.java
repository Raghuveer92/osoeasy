package com.osoeasy.models.Resident;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sahil on 7/6/16.
 */


public class Table1 {

        @SerializedName("mb_mbid")
        @Expose
        private String mb_mbid;
        @SerializedName("mb_mbcode")
        @Expose
        private String mb_mbcode;
        @SerializedName("mb_mbdate")
        @Expose
        private String mb_mbdate;
        @SerializedName("mb_password")
        @Expose
        private String mb_password;
        @SerializedName("mb_name")
        @Expose
        private String mb_name;
        @SerializedName("mb_dob")
        @Expose
        private String mb_dob;
        @SerializedName("mb_fatharname")
        @Expose
        private String mb_fatharname;
        @SerializedName("mb_regmobileno")
        @Expose
        private String mb_regmobileno;
        @SerializedName("mb_altmobileno")
        @Expose
        private String mb_altmobileno;
        @SerializedName("mb_email")
        @Expose
        private String mb_email;
        @SerializedName("mb_indian")
        @Expose
        private Boolean mb_indian;
        @SerializedName("mb_foreign")
        @Expose
        private Boolean mb_foreign;
        @SerializedName("mb_physically")
        @Expose
        private Boolean mb_physically;
        @SerializedName("mb_freedomf")
        @Expose
        private Boolean mb_freedomf;
        @SerializedName("mb_exserviceman")
        @Expose
        private Boolean mb_exserviceman;
        @SerializedName("mb_socialactivities")
        @Expose
        private Boolean mb_socialactivities;
        @SerializedName("mb_idproof")
        @Expose
        private String mb_idproof;
        @SerializedName("mb_idnumber")
        @Expose
        private String mb_idnumber;
        @SerializedName("mb_restype")
        @Expose
        private Object mb_restype;
        @SerializedName("mb_utid")
        @Expose
        private Object mb_utid;
        @SerializedName("mb_flatno")
        @Expose
        private Object mb_flatno;
        @SerializedName("mb_wnid")
        @Expose
        private Object mb_wnid;
        @SerializedName("mb_rate")
        @Expose
        private Double mb_rate;
        @SerializedName("mb_sqarea")
        @Expose
        private Double mb_sqarea;
        @SerializedName("mb_amount")
        @Expose
        private Double mb_amount;
        @SerializedName("mb_peraddress")
        @Expose
        private String mb_peraddress;
        @SerializedName("mb_occaddress")
        @Expose
        private String mb_occaddress;
        @SerializedName("mb_emgaddress")
        @Expose
        private String mb_emgaddress;
        @SerializedName("mb_remarks")
        @Expose
        private Object mb_remarks;
        @SerializedName("mb_active")
        @Expose
        private Boolean mb_active;
        @SerializedName("mb_paystart")
        @Expose
        private Object mb_paystart;
        @SerializedName("mb_feestop")
        @Expose
        private Object mb_feestop;
        @SerializedName("mb_edtm")
        @Expose
        private String mb_edtm;
        @SerializedName("mb_eby")
        @Expose
        private String mb_eby;
        @SerializedName("mb_mdtm")
        @Expose
        private Object mb_mdtm;
        @SerializedName("mb_mby")
        @Expose
        private Object mb_mby;
        @SerializedName("mb_cdtm")
        @Expose
        private Object mb_cdtm;
        @SerializedName("mb_cby")
        @Expose
        private Object mb_cby;
        @SerializedName("mb_coid")
        @Expose
        private String mb_coid;

        /**
         *
         * @return
         * The mb_mbid
         */
        public String getMb_mbid() {
            return mb_mbid;
        }

        /**
         *
         * @param mb_mbid
         * The mb_mbid
         */
        public void setMb_mbid(String mb_mbid) {
            this.mb_mbid = mb_mbid;
        }

        /**
         *
         * @return
         * The mb_mbcode
         */
        public String getMb_mbcode() {
            return mb_mbcode;
        }

        /**
         *
         * @param mb_mbcode
         * The mb_mbcode
         */
        public void setMb_mbcode(String mb_mbcode) {
            this.mb_mbcode = mb_mbcode;
        }

        /**
         *
         * @return
         * The mb_mbdate
         */
        public String getMb_mbdate() {
            return mb_mbdate;
        }

        /**
         *
         * @param mb_mbdate
         * The mb_mbdate
         */
        public void setMb_mbdate(String mb_mbdate) {
            this.mb_mbdate = mb_mbdate;
        }

        /**
         *
         * @return
         * The mb_password
         */
        public String getMb_password() {
            return mb_password;
        }

        /**
         *
         * @param mb_password
         * The mb_password
         */
        public void setMb_password(String mb_password) {
            this.mb_password = mb_password;
        }

        /**
         *
         * @return
         * The mb_name
         */
        public String getMb_name() {
            return mb_name;
        }

        /**
         *
         * @param mb_name
         * The mb_name
         */
        public void setMb_name(String mb_name) {
            this.mb_name = mb_name;
        }

        /**
         *
         * @return
         * The mb_dob
         */
        public String getMb_dob() {
            return mb_dob;
        }

        /**
         *
         * @param mb_dob
         * The mb_dob
         */
        public void setMb_dob(String mb_dob) {
            this.mb_dob = mb_dob;
        }

        /**
         *
         * @return
         * The mb_fatharname
         */
        public String getMb_fatharname() {
            return mb_fatharname;
        }

        /**
         *
         * @param mb_fatharname
         * The mb_fatharname
         */
        public void setMb_fatharname(String mb_fatharname) {
            this.mb_fatharname = mb_fatharname;
        }

        /**
         *
         * @return
         * The mb_regmobileno
         */
        public String getMb_regmobileno() {
            return mb_regmobileno;
        }

        /**
         *
         * @param mb_regmobileno
         * The mb_regmobileno
         */
        public void setMb_regmobileno(String mb_regmobileno) {
            this.mb_regmobileno = mb_regmobileno;
        }

        /**
         *
         * @return
         * The mb_altmobileno
         */
        public String getMb_altmobileno() {
            return mb_altmobileno;
        }

        /**
         *
         * @param mb_altmobileno
         * The mb_altmobileno
         */
        public void setMb_altmobileno(String mb_altmobileno) {
            this.mb_altmobileno = mb_altmobileno;
        }

        /**
         *
         * @return
         * The mb_email
         */
        public String getMb_email() {
            return mb_email;
        }

        /**
         *
         * @param mb_email
         * The mb_email
         */
        public void setMb_email(String mb_email) {
            this.mb_email = mb_email;
        }

        /**
         *
         * @return
         * The mb_indian
         */
        public Boolean getMb_indian() {
            return mb_indian;
        }

        /**
         *
         * @param mb_indian
         * The mb_indian
         */
        public void setMb_indian(Boolean mb_indian) {
            this.mb_indian = mb_indian;
        }

        /**
         *
         * @return
         * The mb_foreign
         */
        public Boolean getMb_foreign() {
            return mb_foreign;
        }

        /**
         *
         * @param mb_foreign
         * The mb_foreign
         */
        public void setMb_foreign(Boolean mb_foreign) {
            this.mb_foreign = mb_foreign;
        }

        /**
         *
         * @return
         * The mb_physically
         */
        public Boolean getMb_physically() {
            return mb_physically;
        }

        /**
         *
         * @param mb_physically
         * The mb_physically
         */
        public void setMb_physically(Boolean mb_physically) {
            this.mb_physically = mb_physically;
        }

        /**
         *
         * @return
         * The mb_freedomf
         */
        public Boolean getMb_freedomf() {
            return mb_freedomf;
        }

        /**
         *
         * @param mb_freedomf
         * The mb_freedomf
         */
        public void setMb_freedomf(Boolean mb_freedomf) {
            this.mb_freedomf = mb_freedomf;
        }

        /**
         *
         * @return
         * The mb_exserviceman
         */
        public Boolean getMb_exserviceman() {
            return mb_exserviceman;
        }

        /**
         *
         * @param mb_exserviceman
         * The mb_exserviceman
         */
        public void setMb_exserviceman(Boolean mb_exserviceman) {
            this.mb_exserviceman = mb_exserviceman;
        }

        /**
         *
         * @return
         * The mb_socialactivities
         */
        public Boolean getMb_socialactivities() {
            return mb_socialactivities;
        }

        /**
         *
         * @param mb_socialactivities
         * The mb_socialactivities
         */
        public void setMb_socialactivities(Boolean mb_socialactivities) {
            this.mb_socialactivities = mb_socialactivities;
        }

        /**
         *
         * @return
         * The mb_idproof
         */
        public String getMb_idproof() {
            return mb_idproof;
        }

        /**
         *
         * @param mb_idproof
         * The mb_idproof
         */
        public void setMb_idproof(String mb_idproof) {
            this.mb_idproof = mb_idproof;
        }

        /**
         *
         * @return
         * The mb_idnumber
         */
        public String getMb_idnumber() {
            return mb_idnumber;
        }

        /**
         *
         * @param mb_idnumber
         * The mb_idnumber
         */
        public void setMb_idnumber(String mb_idnumber) {
            this.mb_idnumber = mb_idnumber;
        }

        /**
         *
         * @return
         * The mb_restype
         */
        public Object getMb_restype() {
            return mb_restype;
        }

        /**
         *
         * @param mb_restype
         * The mb_restype
         */
        public void setMb_restype(Object mb_restype) {
            this.mb_restype = mb_restype;
        }

        /**
         *
         * @return
         * The mb_utid
         */
        public Object getMb_utid() {
            return mb_utid;
        }

        /**
         *
         * @param mb_utid
         * The mb_utid
         */
        public void setMb_utid(Object mb_utid) {
            this.mb_utid = mb_utid;
        }

        /**
         *
         * @return
         * The mb_flatno
         */
        public Object getMb_flatno() {
            return mb_flatno;
        }

        /**
         *
         * @param mb_flatno
         * The mb_flatno
         */
        public void setMb_flatno(Object mb_flatno) {
            this.mb_flatno = mb_flatno;
        }
}
