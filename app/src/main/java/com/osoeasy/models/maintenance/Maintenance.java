package com.osoeasy.models.maintenance;

/**
 * Created by Admin on 05-Aug-16.
 */
public class Maintenance {
    private int SrlNo ;
    private String ScheduleNo ;
    private String Status ;
    private String ScheduleDate ;
    private String AssetCode ;
    private String AssetName ;
    private String ServiceDescription ;
    private String Remarks ;
    private String SocietyName ;

    public int getSrlNo() {
        return SrlNo;
    }

    public String getScheduleNo() {
        return ScheduleNo;
    }

    public String getStatus() {
        return Status;
    }

    public String getScheduleDate() {
        return ScheduleDate;
    }

    public String getAssetCode() {
        return AssetCode;
    }

    public String getAssetName() {
        return AssetName;
    }

    public String getServiceDescription() {
        return ServiceDescription;
    }

    public String getRemarks() {
        return Remarks;
    }

    public String getSocietyName() {
        return SocietyName;
    }
}
