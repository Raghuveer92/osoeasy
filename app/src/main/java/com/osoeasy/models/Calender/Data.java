package com.osoeasy.models.Calender;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahil on 7/7/16.
 */
public class Data {

    @SerializedName("table1")
    @Expose
    private List<Table1> table1 = new ArrayList<Table1>();

    /**
     *
     * @return
     * The userData
     */
    public List<Table1> getTable1() {
        return table1;
    }

    /**
     *
     * @param table1
     * The userData
     */
    public void setTable1(List<Table1> table1) {
        this.table1 = table1;
    }

}

