package com.osoeasy.models.Calender;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.osoeasy.R;
import com.p_v.flexiblecalendar.entity.Event;

import java.io.Serializable;

/**
 * Created by Sahil on 7/7/16.
 */
public class Table1 implements Event, Serializable {



    @SerializedName("ca_usid")
    @Expose
    private String ca_usid;

    @SerializedName("ca_sdate")
    @Expose
    private String ca_sdate;
    @SerializedName("ca_edate")
    @Expose
    private String ca_edate;
    @SerializedName("ca_title")
    @Expose
    private String ca_title;
    @SerializedName("ca_details")
    @Expose
    private String ca_details;
    @SerializedName("ca_public")
    @Expose
    private Boolean ca_public;
    @SerializedName("ca_private")
    @Expose
    private Boolean ca_private;
    @SerializedName("ca_inActive")
    @Expose
    private Object ca_inActive;
    @SerializedName("ca_coid")
    @Expose
    private String ca_coid;

    /**
     *
     * @return
     * The ca_usid
     */
    public String getCa_usid() {
        return ca_usid;
    }

    /**
     *
     * @param ca_usid
     * The ca_usid
     */
    public void setCa_usid(String ca_usid) {
        this.ca_usid = ca_usid;
    }

    /**
     *
     * @return
     * The ca_sdate
     */
    public String getCa_sdate() {
        return ca_sdate;
    }

    /**
     *
     * @param ca_sdate
     * The ca_sdate
     */
    public void setCa_sdate(String ca_sdate) {
        this.ca_sdate = ca_sdate;
    }

    /**
     *
     * @return
     * The ca_edate
     */
    public String getCa_edate() {
        return ca_edate;
    }

    /**
     *
     * @param ca_edate
     * The ca_edate
     */
    public void setCa_edate(String ca_edate) {
        this.ca_edate = ca_edate;
    }

    /**
     *
     * @return
     * The ca_title
     */
    public String getCa_title() {
        return ca_title;
    }

    /**
     *
     * @param ca_title
     * The ca_title
     */
    public void setCa_title(String ca_title) {
        this.ca_title = ca_title;
    }

    /**
     *
     * @return
     * The ca_details
     */
    public String getCa_details() {
        return ca_details;
    }

    /**
     *
     * @param ca_details
     * The ca_details
     */
    public void setCa_details(String ca_details) {
        this.ca_details = ca_details;
    }

    /**
     *
     * @return
     * The ca_public
     */
    public Boolean getCa_public() {
        return ca_public;
    }

    /**
     *
     * @param ca_public
     * The ca_public
     */
    public void setCa_public(Boolean ca_public) {
        this.ca_public = ca_public;
    }

    /**
     *
     * @return
     * The ca_private
     */
    public Boolean getCa_private() {
        return ca_private;
    }

    /**
     *
     * @param ca_private
     * The ca_private
     */
    public void setCa_private(Boolean ca_private) {
        this.ca_private = ca_private;
    }

    /**
     *
     * @return
     * The ca_inActive
     */
    public Object getCa_inActive() {
        return ca_inActive;
    }

    /**
     *
     * @param ca_inActive
     * The ca_inActive
     */
    public void setCa_inActive(Object ca_inActive) {
        this.ca_inActive = ca_inActive;
    }

    /**
     *
     * @return
     * The ca_coid
     */
    public String getCa_coid() {
        return ca_coid;
    }

    /**
     *
     * @param ca_coid
     * The ca_coid
     */
    public void setCa_coid(String ca_coid) {
        this.ca_coid = ca_coid;
    }

    @Override
    public int getColor() {
        return R.color.complain_color;
    }
}

