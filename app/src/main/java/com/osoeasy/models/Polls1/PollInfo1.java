package com.osoeasy.models.Polls1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 11-07-2016.
 */
public class PollInfo1 {
    public String getPollId() {
        return pollId;
    }

    public String getPublishdate() {
        return publishdate;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public String getStatus() {
        return status;
    }

    public String getTopic() {
        return topic;
    }

    public String getVoteCount() {
        return voteCount;
    }

    @SerializedName("Pollid")
    String pollId;

    @SerializedName("PublishDate")
    String publishdate;

    @SerializedName("CloseDate")
    String closeDate;

    @SerializedName("Status")
    String status;

    @SerializedName("Topic")
    String topic;

    @SerializedName("VoteCount")
    String voteCount;
}
