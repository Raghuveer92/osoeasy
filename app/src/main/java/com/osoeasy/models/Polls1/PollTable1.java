package com.osoeasy.models.Polls1;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RAHU on 11-07-2016.
 */
public class PollTable1 {
    @SerializedName("table1")
    List<PollInfo1> pollInfo1List;

    public List<PollInfo1> getPollInfo1List() {
        return pollInfo1List;
    }

    public void setPollInfo1List(List<PollInfo1> pollInfo1List) {
        this.pollInfo1List = pollInfo1List;
    }
}
