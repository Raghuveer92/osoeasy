package com.osoeasy.models.Polls1;

import com.osoeasy.models.BaseApi;

/**
 * Created by RAHU on 11-07-2016.
 */
public class PollData1 extends BaseApi {
    PollTable1 data;

    public PollTable1 getData() {
        return data;
    }

    public void setData(PollTable1 data) {
        this.data = data;
    }
}
