package com.osoeasy.CalenderView;

import android.content.Context;
import android.util.AttributeSet;

import com.p_v.flexiblecalendar.entity.Event;
import com.p_v.flexiblecalendar.view.BaseCellView;
import com.p_v.flexiblecalendar.view.CircularEventCellView;

import java.util.List;

/**
 * Created by Sahil on 7/7/16.
 */
public class SquareCellView extends CircularEventCellView {
    public SquareCellView(Context context) {
        super(context);
    }

    public SquareCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareCellView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //making sure the cell view is a square
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
